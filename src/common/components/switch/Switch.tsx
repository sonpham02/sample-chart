import { css } from 'emotion';
import * as React from "react";

interface SwitchProps { onChange: any; value: boolean; }

const container = css`
    padding: 2px;
    width: 50px;
    height: 26px;
    border-radius: 60px;
    position: relative;
    transition: background-color .25s;
    cursor: pointer;

    &.on {
        background: #7DBC36;
        
        .toggle {
            left: 3px;
        }
    }
    
    &.off {
        background: #ddd;
        
        .toggle {
            left: 25px;
        }
    }
    
    .toggle {
        background white;
        position: absolute;
        width: 22px;
        height: 22px;
        top: 4px;
        border-radius: 100%;
        transition: left .25s;
    }
`;

export class Switch extends React.Component<SwitchProps, {}> {
    public handleOnChange = () => {
        this.props.onChange(!this.props.value);
    };

    public render () {
        const { value } = this.props;

        return (
            <div
                className={`${container} ${value ? `on` : `off`}`}
                onClick={this.handleOnChange}
            >
                <div className={`toggle`}/>
            </div>
        )
    }
}
