import * as React from "react";

interface Props {
    scale: any
}

export class AxisLeft extends React.Component<Props, {}> {
    public elem: any;

    public componentWillReceiveProps(nextProps: any) {
        // @ts-ignore
        d3.select(this.elem).call(d3.axisLeft(nextProps.scale))
    }

    public componentDidMount() {
        // @ts-ignore
        d3.select(this.elem).call(d3.axisLeft(this.props.scale))
    }

    public render () {
        return (
            <g className={"axis-left"}
               textAnchor="end"
               ref={elem => this.elem = elem}
            />
        )
    }
}
