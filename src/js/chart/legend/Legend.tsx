import {css} from "emotion";
import * as React from "react";


interface Props {
    text: any,
    size: any,
    styleProps: any
}

const legendContainer = css`
    display: flex;
    align-items: center;
    font-size: 12px
`;

const legend = (size: any) => css`
    height: ${size}px;
    width: ${size}px;
    margin-right: 5px
`;


export class Legend extends React.Component<Props, {}> {
    public render () {
        const {text, styleProps, size} = this.props;
        return (
            <div className={legendContainer}>
                <div className={legend(size)} style={styleProps}/>
                {text}
            </div>
        )
    }
}
